# Phones parser

Script for collecting real phones numbers.

## Install

```
npm install
cp .env.example .env
```

Then fill in the required variables in the `.env` file and run database migrations and seeders:

```
npx sequelize db:migrate
npx sequelize db:seed:all
```

## Run script manually

```
node index.js
```

## Run tests

```
npm test
```

## Development

```
# make migration
npx sequelize model:generate --name ModelName --attributes parent_id:INTEGER,name:STRING

# rollback migration
npx sequelize db:migrate:undo

# add seeder
npx sequelize seed:generate --name seeder-name
```
