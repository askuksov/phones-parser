const fs = require('fs');
const path = require('path');
const assert = require('assert');
const htmlParser = require('../src/parsers/receive-sms-free');

describe('Phones Parser for receive-sms-free', function () {
  context('Test HTML parser', function () {
    it('should return valid json response when parsing regions HTML page.', function () {
      const html = fs.readFileSync(path.join(__dirname, 'assets', 'receive-sms-free.regions.test.html'), 'utf8');
      const json = require('./assets/receive-sms-free.regions.test.json');
      assert.deepStrictEqual(htmlParser(html), json);
    });

    it('should return valid json response when parsing phones HTML page.', function () {
      const html = fs.readFileSync(path.join(__dirname, 'assets', 'receive-sms-free.phones.test.html'), 'utf8');
      const json = require('./assets/receive-sms-free.phones.test.json');
      assert.deepStrictEqual(htmlParser(html, 'United States'), json);
    });
  });
});
