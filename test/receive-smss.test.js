const fs = require('fs');
const path = require('path');
const assert = require('assert');
const htmlParser = require('../src/parsers/receive-smss');

describe('Phones Parser for receivesmss', function () {
  context('Test HTML parser', function () {
    it('should return valid json response when parsing non empty HTML page.', function () {
      const html = fs.readFileSync(path.join(__dirname, 'assets', 'receivesmss.test.html'), 'utf8');
      const json = require('./assets/receivesmss.test.json');
      assert.deepStrictEqual(htmlParser(html), json);
    });
  });
});
