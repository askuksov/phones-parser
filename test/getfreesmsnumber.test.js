const fs = require('fs');
const path = require('path');
const assert = require('assert');
const htmlParser = require('../src/parsers/getfreesmsnumber');

describe('Phones Parser for getfreesmsnumber', function () {
  context('Test HTML parser', function () {
    it('should return valid json response when parsing non empty HTML page.', function () {
      const html = fs.readFileSync(path.join(__dirname, 'assets', 'getfreesmsnumber.test.html'), 'utf8');
      const json = require('./assets/getfreesmsnumber.test.json');
      assert.deepStrictEqual(htmlParser(html), json);
    });
  });
});
