const getRandomInt = require('./getRandomInt');

module.exports = (attempt) => attempt ** 2 * 1000 + getRandomInt(1, 1000);
