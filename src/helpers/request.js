const wait = require('./wait');
const getDelay = require('./getDelay');
const noproxy = require('../proxies/noproxy');
const proxycrawl = require('../proxies/proxycrawl');
const {
  PROXYCRAWL_TOKEN,
  MAX_RETRY_ATTEMPTS,
} = require('../config/main');

async function request(url, attempt = 1) {
  try {
    if (PROXYCRAWL_TOKEN && PROXYCRAWL_TOKEN.length) {
      const { original_status, pc_status, data } = await proxycrawl(url);
      console.log({ original_status, pc_status, url });

      if (original_status === '200' && pc_status === '200') {
        return data;
      } else if (pc_status === '429' && attempt <= MAX_RETRY_ATTEMPTS) {
        await wait(getDelay(attempt));
        return request(url, attempt + 1);
      } else if (attempt <= MAX_RETRY_ATTEMPTS) {
        await wait(getDelay(attempt));
        const html = await noproxy(url);
        return html;
      }
      throw new Error(`Something went wrong! original_status: ${original_status}, pc_status: ${pc_status}, url: ${url}`);
    } else {
      const html = await noproxy(url);
      return html;
    }
  } catch (error) {
    console.log(error.message);
    if (attempt <= MAX_RETRY_ATTEMPTS) {
      await wait(getDelay(attempt));
      return request(url, attempt + 1);
    }
    return false;
  }
};

module.exports = request;
