const parser = require('../parsers/receive-sms-online');
const request = require('../helpers/request');

let phoneNumbers = [];

async function getData(url = 'https://receive-sms-online.info') {
  try {
    const html = await request(url);
    if (!html || !html.length) {
      throw new Error('empty html');
    }

    const data = parser(html);
    if (!data) {
      throw new Error('empty data');
    }

    if (data.phones && data.phones.length) {
      phoneNumbers = [...new Map([ ...new Set(phoneNumbers.concat(data.phones))].map(item => [item['number'], item])).values()];
    }

    return phoneNumbers;
  } catch (error) {
    console.log(error.message);
    return phoneNumbers;
  }
};

module.exports = getData;
