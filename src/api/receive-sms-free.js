const parser = require('../parsers/receive-sms-free');
const request = require('../helpers/request');
const wait = require('../helpers/wait');

const visitedPages = {
  'https://receive-sms-free.cc/regions/': false,
};
const visitedCountriesPages = {};
let phoneNumbers = [];

async function getData(url = 'https://receive-sms-free.cc/regions/', country = false) {
  console.log({url, country})
  try {
    const html = await request(url);
    if (!html || !html.length) {
      throw new Error('empty html');
    }

    const data = parser(html, country);
    if (!data) {
      throw new Error('empty data');
    }

    if (country) {
      if (visitedCountriesPages[country][url] !== undefined) {
        visitedCountriesPages[country][url] = true;
      }

      if (data.phones && data.phones.length) {
        phoneNumbers = [...new Map([ ...new Set(phoneNumbers.concat(data.phones))].map(item => [item['number'], item])).values()];
      }
    } else {
      if (visitedPages[url] !== undefined) {
        visitedPages[url] = true;
      }

      if (data.countries && data.countries.length) {
        for (const row of data.countries) {
          if (visitedCountriesPages[row.name]) {
            visitedCountriesPages[row.name][row.url] = false;
          } else {
            visitedCountriesPages[row.name] = {
              [row.url]: false,
            };
          }
        }
      }
    }


    if (data.pages && data.pages.length) {
      for (const page of data.pages) {
        if (country && visitedCountriesPages[country][page] === undefined) {
          visitedCountriesPages[country][page] = false;
        } else if (!country && visitedPages[page] === undefined) {
          visitedPages[page] = false;
        }
      }
    }

    for (const pageUrl in visitedPages) {
      if (!visitedPages[pageUrl]) {
        await wait(1000);
        return getData(pageUrl);
      }
    }

    for (const countryName in visitedCountriesPages) {
      for (const pageUrl in visitedCountriesPages[countryName]) {
        if (!visitedCountriesPages[countryName][pageUrl]) {
          await wait(1000);
          return getData(pageUrl, countryName);
        }
      }
    }

    return phoneNumbers;
  } catch (error) {
    console.log(error.message);
    return phoneNumbers;
  }
};

module.exports = getData;
