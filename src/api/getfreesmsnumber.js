const parser = require('../parsers/getfreesmsnumber');
const request = require('../helpers/request');
const wait = require('../helpers/wait');

const visitedPages = {};
let phoneNumbers = [];

async function getData(url = 'https://getfreesmsnumber.com') {
  try {
    const html = await request(url);
    if (!html || !html.length) {
      throw new Error('empty html');
    }

    const data = parser(html);
    if (!data) {
      throw new Error('empty data');
    }

    if (visitedPages[url] !== undefined) {
      visitedPages[url] = true;
    }

    if (data.phones && data.phones.length) {
      phoneNumbers = [...new Map([ ...new Set(phoneNumbers.concat(data.phones))].map(item => [item['number'], item])).values()];
    }
    if (data.pages && data.pages.length) {
      for (const page of data.pages) {
        if (visitedPages[page] === undefined) {
          visitedPages[page] = false;
        }
      }
    }

    for (const pageUrl in visitedPages) {
      if (!visitedPages[pageUrl]) {
        await wait(1000);
        return getData(pageUrl);
      }
    }

    return phoneNumbers;
  } catch (error) {
    console.log(error.message);
    return phoneNumbers;
  }
};

module.exports = getData;
