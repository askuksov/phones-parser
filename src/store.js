const {
  Country,
  Phone,
  Source,
} = require('./models');

module.exports = async (results) => {
  const mapCountries = results.map(r => r.country).reduce((a, n) => {
    a[n] = null;
    return a;
  }, {});

  const dataCountries = Object.keys(mapCountries).map(name => ({ name }));

  await Country.bulkCreate(dataCountries, {
    fields: ['name'],
    ignoreDuplicates: true,
  });

  const countries = await Country.findAll();
  for (const country of countries) {
    if (mapCountries[country.name] !== undefined) {
      mapCountries[country.name] = country.id;
    }
  }

  const mapSources = results.map(r => r.source).reduce((a, n) => {
    a[n] = null;
    return a;
  }, {});

  const dataSources = Object.keys(mapSources).map(name => ({ name }));

  await Source.bulkCreate(dataSources, {
    fields: ['name'],
    ignoreDuplicates: true,
  });

  const sources = await Source.findAll();
  for (const source of sources) {
    if (mapSources[source.name] !== undefined) {
      mapSources[source.name] = source.id;
    }
  }

  const dataPhones = results.map(r => {
    return {
      sourceId: mapSources[r.source],
      countryId: mapCountries[r.country],
      number: r.number,
    };
  });

  await Phone.bulkCreate(dataPhones, {
    fields: ['sourceId', 'countryId', 'number'],
    ignoreDuplicates: true,
  });
};
