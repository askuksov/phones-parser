const cheerio = require('cheerio');

module.exports = (html, country = false) => {
  const result = {
    countries: [],
    phones: [],
    pages: [],
  };

  const $ = cheerio.load(html);

  $('#ul li').each((i, card) => {
    const text = $(card).find('h2 span');
    const url = $(card).find('a');

    if (text && text.length && url && url.length) {
      if (country) {
        result.phones.push({
          country: country,
          number: text.text().replace(/\s/g, '').trim(),
          source: 'receive-sms-free',
        });
      } else {
        result.countries.push({
          name: text.text().replace(/phone number/gi, '').trim(),
          url: url.attr('href'),
        });
      }
    }
  });

  $('.pagination a').each((i, link) => {
    if ($(link).attr('href')) {
      result.pages.push($(link).attr('href'));
    }
  });

  return result;
};
