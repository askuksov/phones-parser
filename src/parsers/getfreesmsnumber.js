const cheerio = require('cheerio');

module.exports = (html) => {
  const result = {
    phones: [],
    pages: [],
  };

  const $ = cheerio.load(html);

  $('.card').each((i, card) => {
    const country = $(card).find('h3');
    const phone = $(card).find('p.font-weight-bold');

    if (country && country.length && phone && phone.length) {
      result.phones.push({
        country: country.text().trim(),
        number: phone.text().trim(),
        source: 'getfreesmsnumber',
      });
    }
  });

  $('.pagination a').each((i, link) => {
    result.pages.push($(link).attr('href'));
  });

  return result;
};
