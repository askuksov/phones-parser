const cheerio = require('cheerio');

module.exports = (html) => {
  const result = {
    phones: [],
    pages: [],
  };

  const $ = cheerio.load(html);

  $('.number-boxes .number-boxes-item').each((i, card) => {
    const country = $(card).find('.number-boxess-item-country');
    const phone = $(card).find('.number-boxes-itemm-number');

    if (country && country.length && phone && phone.length) {
      result.phones.push({
        country: country.text().trim(),
        number: phone.text().trim(),
        source: 'receive-smss',
      });
    }
  });

  return result;
};
