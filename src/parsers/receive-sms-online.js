const cheerio = require('cheerio');

const countriesMapping = {
  'USA': 'United States',
};

module.exports = (html) => {
  const result = {
    phones: [],
    pages: [],
  };

  const $ = cheerio.load(html);

  $('.Table .Cell div').each((i, card) => {
    const country = $(card).contents().first().text();
    const phone = $(card).find('a');

    if (country && country.length && phone && phone.length) {
      result.phones.push({
        country: countriesMapping[country.trim()] ? countriesMapping[country.trim()] : country.trim(),
        number: phone.text().trim(),
        source: 'receive-sms-online',
      });
    }
  });

  return result;
};
