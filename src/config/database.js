require('dotenv').config();

const {
  DB_HOST = '127.0.0.1',
  DB_PORT = '3306',
  DB_USER = 'root',
  DB_PASS,
  DB_NAME = 'phones_parser',
} = process.env;

module.exports = {
  host: DB_HOST,
  port: DB_PORT,
  username: DB_USER,
  password: DB_PASS,
  database: DB_NAME,
  dialect: 'mysql',
  logging: false,
};
