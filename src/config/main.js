const {
  PROXYCRAWL_TOKEN,
  MAX_RETRY_ATTEMPTS = 3,
} = process.env;

module.exports = {
  PROXYCRAWL_TOKEN,
  MAX_RETRY_ATTEMPTS: parseInt(MAX_RETRY_ATTEMPTS),
};
