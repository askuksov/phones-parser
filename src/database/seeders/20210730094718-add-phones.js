const store = require('../../store');
const phones = require('../phones');

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await store(phones);
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
