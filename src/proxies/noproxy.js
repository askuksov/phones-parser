const axios = require('axios');
const UserAgent = require('user-agents');

module.exports = async (url) => {
  const ua = new UserAgent({ deviceCategory: 'desktop' });
  const options = {
    url,
    method: 'GET',
    headers: {
      'User-Agent': ua.toString(),
    },
  };

  const { data } = await axios(options);
  return data;
};
