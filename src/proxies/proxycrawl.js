const axios = require('axios');
const {
  PROXYCRAWL_TOKEN,
} = require('../config/main');

module.exports = async (url) => {
  const options = {
    url: 'https://api.proxycrawl.com',
    params: {
      token: PROXYCRAWL_TOKEN,
      device: 'desktop',
      url,
    },
  };

  const { headers: { original_status, pc_status }, data } = await axios(options);
  return { original_status, pc_status, data };
};
