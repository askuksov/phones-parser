require('dotenv').config();

const apis = require('./src/api');
const store = require('./src/store');

(async () => {
  try {
    const promises = [];
    for (const api of apis) {
      promises.push(api());
    }
    const results = await Promise.all(promises);

    console.log(JSON.stringify(results, null, 2));
    await store(results.flat());
  } catch (error) {
    console.log(error);
  }
})();
